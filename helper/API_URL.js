export default {
    BASE_URL: 'http://192.168.100.7:8022/api',


  // ######### Room ############
  API_FETCH_ROOM: '/room',
  API_FETCH_ROOM_INFO: '/room/show/',
  API_ADD_ROOM: '/room/update',
  API_DELETE_ROOM: '/room/show/',


  // ######### ASSET ############
  API_FETCH_ASSET: '/asset',
  API_FETCH_ASSET_INFO: '/asset/show/',
  // API_ADD_ASSET: '/asset/update',
  // API_ADD_ASSET: '/asset/store',


  // ########## BOOKING ############
  API_FETCH_BOOKING:'/booking',

  // ########## REPORT ############
  API_FETCH_BILL:'/check_in_out/print_bill',

  // ########## CHECK IN-OUT ############
  API_FETCH_CHECK_IN_OUT_REPORT:'/check_in_out/report_check_in_out',

  // ########## EMPLOYEE ############
  API_FETCH_EMPLOYEE:'/employee',
  API_FETCH_EMPLOYEE_INFO: '/employee/show/',
  API_ADD_EMPLOYEE: '/employee',

  // ########## CUSTOMER ############
  API_FETCH_CUSTOMER: '/customer',
  API_ADD_CUSTOMER: '/customer',
  API_CUSTOMER_INFO: '/customer',
}
