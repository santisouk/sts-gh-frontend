import Swal from 'sweetalert2'





export const swalError = (swal, title, message) => {

  swal({
    icon: 'error',
    title: title,
    text: message || "ມີບາງຢ່່າງຜິດພາດ ກະລຸນາລອງໃໝ່ອີກຄັ້ງ",
    showConfirmButton: true,
    allowEscapeKey: false,
    allowOutsideClick: false,
    allowEnterKey: false,
    // html: `
    //               <span>
    //                 <span style="font-weight: bold;">Trace ID:</span><span style="color: red"> ${trandId}</span> <br />
    //                 <span style="font-weight: bold;">Message:</spn><span style="color: red"> ${message}</span>
    //               </span>
    //             `
  })
}


export const swalSuccess = (swal, title, text) => {
  swal({
    icon: 'success',
    title: title,
    text: text,
    showConfirmButton: true,
    allowEscapeKey: false,
    allowOutsideClick: false,
    allowEnterKey: false
  })
}

export const closeSwal = (swal) => {
  Swal.hideLoading()
  Swal.close()
}

export const swalLoading = (swal, title, text, icon) => {
  Swal.fire({
    title: title,
    text: text,
    // html: html,
    icon: icon,
    allowEscapeKey: false,
    allowOutsideClick: false,
    allowEnterKey: false,
    onBeforeOpen: () => {
      Swal.showLoading()
    }
  })
}

export const swalConfirm = (swal, title, text, icon, cancelButtonText, confirmButtonText) => {
  return Swal.fire({
    title: title,
    text: text,
    icon: icon,
    allowEscapeKey: false,
    allowOutsideClick: false,
    allowEnterKey: false,
    showCancelButton: true,
    cancelButtonText: cancelButtonText || 'ບໍ່',
    confirmButtonText: confirmButtonText || 'ຕົກລົງ'
  })
}

export const swalSuccessBackToMenu = (swal, message, transt24Id, status) => {
  return swal({
    title: 'ແຈ້ງເຕືອນ',
    icon: 'success',
    html: `<span>
                <span style="font-weight: bold;">ເລກທຸລະກຳ:</span><span style="color: orangered"> ${transt24Id}</span><br/>
                <span style="font-weight: bold;">ສະຖານະທຸລະກຳ:</spn>
                <span style="color: ${status === 'AUTHORISE' ? 'green' : 'red'}"> ${message}</span>
            </span>
    `,
    allowEscapeKey: false,
    allowOutsideClick: false,
    allowEnterKey: false,
    confirmButtonText: 'ກັບສູ່ເມນູຕ່າງໆ'
  })
}

export const toastSuccess = (swal, title) => {
  swal({
    toast: true,
    icon: 'success',
    position: 'top-right',
    showConfirmButton: false,
    timerProgressBar: true,
    timer: 3000,
    title: title
  })
}

export const swalWarning = (swal, title, message) => {
  swal({
    icon: 'error',
    title: title,
    text: message || 'ມີບາງຢ່່າງຜິດພາດ ກະລຸນາລອງໃໝ່ອີກຄັ້ງ',
    showConfirmButton: true,
    allowEscapeKey: false,
    allowOutsideClick: false,
    allowEnterKey: false
  })
}

export const swalMsg = (swal, icon, title, message) => {
  swal({
    icon: icon,
    title: title,
    text: message,
    showConfirmButton: true,
    allowEscapeKey: false,
    allowOutsideClick: false,
    allowEnterKey: false
  })
}







