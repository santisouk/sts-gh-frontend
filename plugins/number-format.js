import Vue from 'vue'
import number from 'numeral';
import numFormat from 'vue-filter-number-format';

Vue.filter('numFormat', numFormat(number));
