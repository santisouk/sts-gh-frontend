import API_URL from "~/helper/API_URL";
import {swalError, swalSuccess} from "~/helper/swal"



const state = () => ({
  roomList: [],
  roomInfo: '',
  isLoading:false,
});

const actions = {
  async FETCH_ROOM(context) {
    try {
      context.commit('SET_LOADING', true)
      const res = await this.$axios.get(API_URL.API_FETCH_ROOM)
      if (res.data.status === "success") {
        context.commit('SET_LOADING', false)
        context.commit('SET_ROOM', res.data.dataResponse)
      } else {
        context.commit('SET_LOADING', false)
        swalError(this.$swal, 'ເກີດຂໍ້ຜິດພາດ', 'ບໍ່ສາມາດດືງຂໍ້ມູນໄດ້', 'error')
      }
    } catch (error) {
      context.commit('SET_LOADING', false)
      swalError(this.$swal, 'ເກີດຂໍ້ຜິດພາດ', error)
    }
  },


  async FETCH_ROOM_INFO({commit}, id) {
    try {
      commit('SET_LOADING', true)
      commit('SET_ROOM_INFO', "")
      const res = await this.$axios.get(API_URL.API_FETCH_ROOM_INFO + `${id}`);
      console.log("data status:",res.data.dataResponse)
      if (res.data.status == 'success') {
        commit('SET_LOADING', false)
        commit('SET_ROOM_INFO', res.data.dataResponse)
      } else if (res.status !== 'success') {
        commit('SET_LOADING', false)
        console.log("error", 'Can\'t fetch data!')
      }
    } catch (error) {
      commit('SET_LOADING', false)
      console.log(error)
    }
  },

  async DELETE_ROOM_INFO({commit}, id) {
    try {
      commit('SET_LOADING', true)
      context.commit('SET_ROOM', "")
      await this.$axios.$get(API_URL.API_DELETE_ROOM + `${id}`);
      if (res.status === 'success') {
        const res = await this.$axios.get(API_URL.API_FETCH_ROOM)
        commit('SET_LOADING', false)
        context.commit('SET_ROOM', res.data.dataResponse)
      } else if (res.status !== 'success') {
        commit('SET_LOADING', false)
        console.log("error", 'Can\'t fetch data!')
      }
    } catch (error) {
      commit('SET_LOADING', false)
      console.log(error)
    }
  },


  // async update_patient(context, payload) {
  //   try {
  //     const {formData, patientId} = payload
  //     const res = await this.$axios.put(API_URL.API_URL_PATIENT`/${payload.patientId}`, payload, {
  //       headers: {
  //         'x-clinic-apiKeyHeadAuth': process.env.appKey,
  //         'Content-Type': 'multipart/form-data'
  //       }
  //     });
  //     if (res.data) {
  //       swalSuccess(this.$swal, 'ສຳເລັດ', 'ບັນທືກຂໍ້ມູນສຳເລັດ')
  //     } else {
  //       swalError(this.$swal, 'ແຈ້ງເຕືອນ', 'ເກີດຂໍ້ຜີີດພາດບັນທືກຂໍ້ມູນ')
  //       context.commit('HANDLE_ERROR', [])
  //     }
  //   } catch (error) {
  //     context.commit('SET_LOADING', false)
  //     context.commit('HANDLE_ERROR', 'Something went wrong')
  //     swalError(this.$swal, 'ເກີດຂໍ້ຜິດພາດ', error)
  //   }
  // }


};


const mutations = {

  SET_ROOM(state, payload) {
    state.roomList = payload
  },

  SET_ROOM_INFO(state, payload) {
    state.roomInfo = payload
  },

  SET_LOADING(state, payload) {
    state.isLoading = payload
  },

};

const getters = {
  GETTER_ROOM(state) {
    return state.roomList
  },

  GETTER_ROOM_INFO(state) {
    return state.roomInfo
  },

  GETTER_LOAD(state) {
    return state.isLoading
  },
};

export default {
  state,
  getters,
  mutations,
  actions
}
