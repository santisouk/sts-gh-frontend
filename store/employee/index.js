import API_URL from "~/helper/API_URL";
import {swalError} from "~/helper/swal"


const state = () => ({
  employees: [],
  employeeInfo: {},
  isLoading:false
});

const actions = {
  async FETCH_EMPLOYEE(context) {
    try {
      const res = await this.$axios.get(API_URL.API_FETCH_EMPLOYEE)
      if (res.data.status === "success") {
        context.commit('SETTER_EMPLOYEE', res.data.dataResponse)
        // console.log(res.data.dataResponse);
      } else {
        swalError(this.$swal, 'ເກີດຂໍ້ຜິດພາດ', 'ບໍ່ສາມາດດືງຂໍ້ມູນໄດ້', 'error')
      }
    } catch (error) {
      swalError(this.$swal, 'ເກີດຂໍ້ຜິດພາດ', error)
    }
  },


  async FETCH_EMPLOYEE_INFO({commit}, id) {
    try {
      commit('SETTER_LOADING', true)
      const res = await this.$axios.get(API_URL.API_FETCH_EMPLOYEE_INFO+ `${id}`)
      if (res.data.status === "success") {
        commit('SETTER_EMPLOYEE_INFO', res.data.dataResponse)
        commit('SETTER_LOADING', false)

      } else {
        commit('SETTER_LOADING', false)
        swalError(this.$swal, 'ເກີດຂໍ້ຜິດພາດ', 'ບໍ່ສາມາດດືງຂໍ້ມູນໄດ້', 'error')
      }
    } catch (error) {
      commit('SETTER_LOADING', false)
      swalError(this.$swal, 'ເກີດຂໍ້ຜິດພາດ', error)
    }
  },



};


const mutations = {
  SETTER_EMPLOYEE(state, payload) {
    state.employees = payload
  },
  SETTER_EMPLOYEE_INFO(state, payload) {
    state.employeeInfo = payload
  },

  SETTER_LOADING(state, payload) {
    state.isLoading = payload
  },
};

const getters = {
  GETTER_EMPLOYEE(state) {
    return state.employees
  },

  GETTER_EMPLOYEE_INFO(state) {
    return state.employeeInfo
  },

  GETTER_LOADING(state) {
    return state.isLoading
  }



};

export default {
  state,
  getters,
  mutations,
  actions
}
