import API_URL from "~/helper/API_URL";
import {swalError} from "~/helper/swal"


const state = () => ({
  checkInOuts: [],
});

const actions = {
  async FETCH_CHECK_IN_OUT({commit}, payload) {
    try {
      const res = await this.$axios.post(API_URL.API_FETCH_CHECK_IN_OUT_REPORT, payload)
      if (res.data.status === "success") {
        commit('SETTER_CHECK_IN_OUT', res.data.dataResponse)
      } else {
        swalError(this.$swal, 'ເກີດຂໍ້ຜິດພາດ', 'ບໍ່ສາມາດດືງຂໍ້ມູນໄດ້', 'error')
      }
    } catch (error) {
      swalError(this.$swal, 'ເກີດຂໍ້ຜິດພາດ', error)
    }
  },


};


const mutations = {
  SETTER_CHECK_IN_OUT(state, payload) {
    state.checkInOuts = payload
  },
};

const getters = {
  GETTER_CHECK_IN_OUT(state) {
    return state.checkInOuts
  },

};

export default {
  state,
  getters,
  mutations,
  actions
}
