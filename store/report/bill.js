import API_URL from '~/helper/API_URL'
import { swalError } from '~/helper/swal'


const state = () => ({

  bills: [],
  bill_detail: {},
  isLoading: false

})

const actions = {
  async FETCH_BILL({ commit }, payload) {
    try {
      commit('SETTER_LOADING', true)
      commit('SETTER_BILL', '')
      const res = await this.$axios.post(API_URL.API_FETCH_BILL, payload)
      if (res.data.status == 'success') {
        commit('SETTER_LOADING', false)
        commit('SETTER_BILL', res.data.dataResponse)
      } else if (res.status !== 'success') {
        commit('SETTER_LOADING', false)
        console.log('error', 'Can\'t fetch data!')
      }
    } catch (error) {
      context.commit('SETTER_LOADING', false)
      console.log(error)
    }
  }
}


const mutations = {
  SETTER_BILL(state, payload) {
    state.bills = payload
  },

  SETTER_LOADING(state, payload) {
    state.isLoading = payload
  }
}

const getters = {
  GETTER_BILL(state) {
    return state.bills
  },

  GETTER_LOADING(state) {
    return state.isLoading
  }

}

export default {
  state,
  getters,
  mutations,
  actions
}
