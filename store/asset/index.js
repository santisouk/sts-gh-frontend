import API_URL from "~/helper/API_URL";
import {swalError} from "~/helper/swal"


const state = () => ({
  asset_list: [],
  assetInfo: {},
  isLoading:false,

});

const actions = {
  async FETCH_ASSET(context) {
    try {
      const res = await this.$axios.get(API_URL.API_FETCH_ASSET)
      if (res.data.status === "success") {
        context.commit('SET_ASSET', res.data.dataResponse)
      } else {
        swalError(this.$swal, 'ເກີດຂໍ້ຜິດພາດ', 'ບໍ່ສາມາດດືງຂໍ້ມູນໄດ້', 'error')
      }
    } catch (error) {
      swalError(this.$swal, 'ເກີດຂໍ້ຜິດພາດ', error)
    }
  },

  async FETCH_ASSET_INFO({commit}, id) {
    try {
      commit('SETTER_LOADING', true)
      commit('SETTER_ASSET_INFO', "")
      const res = await this.$axios.get(API_URL.API_FETCH_ASSET_INFO + `${id}`);
      // console.log("data status:",res.data.dataResponse)
      if (res.data.status == 'success') {
        commit('SETTER_LOADING', false)
        commit('SETTER_ASSET_INFO', res.data.dataResponse)
      } else if (res.status !== 'success') {
        commit('SETTER_LOADING', false)
        console.log("error", 'Can\'t fetch data!')
      }
    } catch (error) {
      commit('SETTER_LOADING', false)
      console.log(error)
    }
  },

};


const mutations = {
  SET_ASSET(state, payload) {
    state.asset_list = payload
  },

  SETTER_ASSET_INFO(state, payload) {
    state.assetInfo = payload
  },

  SETTER_LOADING(state, payload) {
    state.isLoading = payload
  },
};

const getters = {
  GETTER_ASSET(state) {
    return state.asset_list
  },

  GETTER_ASSET_INFO(state){
    return state.assetInfo
  },

  GETTER_LOADING(state){
    return state.isLoading
  }
};

export default {
  state,
  getters,
  mutations,
  actions
}
