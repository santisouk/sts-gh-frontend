import API_URL from "~/helper/API_URL";
import {swalError} from "~/helper/swal"


const state = () => ({

  customer_list: [],
  customer_info: {},

});

const actions = {
  async FETCH_CUSTOMER(context) {
    try {
      const res = await this.$axios.get(API_URL.API_FETCH_CUSTOMER)
      if (res.data.status === "success") {
        context.commit('SET_CUSTOMER', res.data.dataResponse)
        console.log(res.data.dataResponse);
      } else {
        swalError(this.$swal, 'ເກີດຂໍ້ຜິດພາດ', 'ບໍ່ສາມາດດືງຂໍ້ມູນໄດ້', 'error')
      }
    } catch (error) {
      swalError(this.$swal, 'ເກີດຂໍ້ຜິດພາດ', error)
    }
  },
  async FETCH_CUSTOMER_INFO(context, id) {
    try {
      const res = await this.$axios.get(API_URL.API_CUSTOMER_INFO+`/${id}`)
      if (res.data.status === "success") {
        context.commit('SET_CUSTOMER_INFO', res.data.dataResponse)
        console.log(res.data.dataResponse);
      } else {
        swalError(this.$swal, 'ເກີດຂໍ້ຜິດພາດ', 'ບໍ່ສາມາດດືງຂໍ້ມູນໄດ້', 'error')
      }
    } catch (error) {
      swalError(this.$swal, 'ເກີດຂໍ້ຜິດພາດ', error)
    }
  },
};


const mutations = {
  SET_CUSTOMER(state, payload) {
    state.customer_list = payload
  },
  SET_CUSTOMER_INFO(state, payload) {
    state.customer_info = payload
  },
};

const getters = {
  GET_CUSTOMER(state) {
    return state.customer_list
  },
  GET_CUSTOMER_INFO(state) {
    return state.customer_info
  },
  GETTER_LOADING(state) {
    return state.isLoading
  }
};

export default {
  state,
  getters,
  mutations,
  actions
}
