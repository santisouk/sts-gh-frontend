import API_URL from "~/helper/API_URL";
import {swalError} from "~/helper/swal"


const state = () => ({
  bookings: [],
  isLoading:false
});

const actions = {
  // async FETCH_BOOKING({commit}, payload) {
  async FETCH_BOOKING({commit}, payload) {
    try {
      commit('SETTER_LOADING', true)

      const res = await this.$axios.post(API_URL.API_FETCH_BOOKING, {
        "startDate":"2022-05-01",
        "endDate":"2022-05-06"
      })
      if (res.data.status === "success") {
        commit('SETTER_BOOKING', res.data.dataResponse)
        commit('SETTER_LOADING', false)
      } else {
        commit('SETTER_LOADING', false)
        swalError(this.$swal, 'ເກີດຂໍ້ຜິດພາດ', 'ບໍ່ສາມາດດືງຂໍ້ມູນໄດ້', 'error')
      }
    } catch (error) {
      commit('SETTER_LOADING', false)
      swalError(this.$swal, 'ເກີດຂໍ້ຜິດພາດ', error)
    }
  },


};


const mutations = {
  SETTER_BOOKING(state, payload) {
    state.bookings = payload
  },
  SETTER_LOADING(state, payload) {
    state.isLoading = payload
  },
};

const getters = {
  GETTER_BOOKING(state) {
    return state.bookings
  },
  GETTER_LOADING(state) {
    return state.isLoading
  }

};

export default {
  state,
  getters,
  mutations,
  actions
}
